const host = "localhost:8080";

let delayedRequestTimeout;
document.getElementById("query").addEventListener("input", search);

function fillScopePicker(sites) {
    let scopePicker = document.getElementById("scope");
    for (let site of sites) {
        let option = document.createElement("option");
        option.innerText = site.name;
        option.value = site.api_site_parameter;
        scopePicker.appendChild(option);
    }
    $('.selectpicker').selectpicker('val', ['stackoverflow']);
    $('.selectpicker').selectpicker('refresh');
}

function getSites() {
    axios
        .get('http://' + host + '/getsites')
        .then(function (response) {
            fillScopePicker(response.data);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function search(event) {
    let query = event.target.value;
    let scopePicker = document.getElementById("scope");
    let scope = getSelectValues(scopePicker).join();

    clearTimeout(delayedRequestTimeout);
    delayedRequestTimeout = setTimeout(() => sendSearchRequest(query, scope), 200);
}

function sendSearchRequest(query, scope) {
    const list = document.getElementById("list");
    const loader = document.getElementById("loader");
    list.style = "display:none;";
    loader.style = "display:block; margin-top: 50px";

    axios
        .get('http://' + host + '/search', { params: { query, scope } })
        .then(function (response) {

            list.style = "display:block";
            loader.style = "display:none";

            showQuestions(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
}

function showQuestions(questions) {

    const list = document.getElementById("list");
    const newList = list.cloneNode(false);
    list.parentNode.replaceChild(newList, list);
    const fragment = document.createDocumentFragment();

    for (let item of questions) {
        let row = createRow(item);
        fragment.appendChild(row);
    }
    newList.appendChild(fragment);
}

function createRow(item) {
    let question = document.createElement("div");
    question.className = "question";

    question.appendChild(createTitle(item.title, item.link));

    if (item.is_answered) {
        question.style = "background-color: rgba(0, 150, 0, 0.1)";
    }

    question.appendChild(createAuthorLink(item.owner.display_name, item.owner.link));
    question.appendChild(createDate(new Date(item.creation_date)));

    return question;
}

function createTitle(titleText, link) {
    let title = document.createElement("h5");
    let linkElement = document.createElement("a");
    linkElement.href = link;
    linkElement.innerText = decodeHTML(titleText);
    linkElement.style = "color: black;"
    title.appendChild(linkElement);
    return title;
}

function createAuthorLink(authorName, profileLink) {

    let author = document.createElement("p");
    author.style = 'margin-top: 20px';

    let link = document.createElement("a");
    link.href = profileLink;
    link.innerText = 'Author: ' + authorName;

    author.appendChild(link);
    return author;
}

function createDate(date) {
    let dateElement = document.createElement("p");

    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    dateElement.innerText = 'Date: ' + date.toLocaleDateString("en-US", options);

    return dateElement;
}

function getSelectValues(select) {
    let result = [];
    let options = select && select.options;
    let opt;

    for (let i = 0, iLen = options.length; i < iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value);
        }
    }
    return result;
}

function decodeHTML(str) {
    return str.replace(/&#(\d+);/g, function(match, dec) {
        return String.fromCharCode(dec);
    });
};