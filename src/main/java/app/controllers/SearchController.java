package app.controllers;

import app.common.Scope;
import app.common.exchangeAPI.StackExchangeAPIClient;
import app.common.exchangeAPI.contract.Question;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
    public class SearchController {

    final StackExchangeAPIClient client;
    final Log logger;

    public SearchController(StackExchangeAPIClient client){
        this.logger = LogFactory.getLog(SearchController.class);
        this.client = client;
    }

    @GetMapping("/search")
    public List<Question> search(
            @RequestParam(name="query") String query,
            @RequestParam(name="scope", required=false, defaultValue="stackoverflow")String scopeString
    ) {
        logger.info("Handling /search request with params: query = " + query + "; scope = " + scopeString);

        Scope scope = Scope.parse(scopeString);
        ArrayList<Question> result = new ArrayList<>();

        for (String site : scope.getItems()) {
            List<Question> foundQuestions = searchSite(query, site);
            result.addAll(foundQuestions);
        }

        logger.info(result.size() + " results found");
        return result;
    }

    private List<Question> searchSite(String query, String site) {

        try {
            Question[] questions = client.search(query, site);
            return Arrays.asList(questions);
        }
        catch (Exception e){
            logger.error("Error has occurred while searching in site " + site + "; query = " + query);
            return new ArrayList<>();
        }
    }
}
