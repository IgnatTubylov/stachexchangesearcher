package app.controllers;

import app.common.exchangeAPI.StackExchangeAPIClient;
import app.common.exchangeAPI.contract.Site;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetSitesController {

    final StackExchangeAPIClient client;
    final Log logger;

    public GetSitesController(StackExchangeAPIClient client) {

        this.logger = LogFactory.getLog(SearchController.class);
        this.client = client;

    }

    @GetMapping("/getsites")
    public Site[] GetSites() {

        logger.info("Handling /getsites request");
        return client.getSites();

    }
}
