package app.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "settings")
public class Settings {

    private String stackExchangeBaseURL;

    public String getStackExchangeBaseURL() {

        return stackExchangeBaseURL;
    }

    public void setStackExchangeBaseURL(String stackExchangeBaseURL) {

        this.stackExchangeBaseURL = stackExchangeBaseURL;
    }
}
