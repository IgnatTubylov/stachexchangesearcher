package app.common;

public class Scope {

    private Scope(String[] items) {

        this.items = items;

    }

    private String[] items;

    public String[] getItems() {

        return items;

    }

    public static Scope parse(String scopeString) {

        String[] items = scopeString.split(",");
        return new Scope(items);
    }
}
