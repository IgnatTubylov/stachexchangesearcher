package app.common.exchangeAPI;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseExtractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

public class StackExchangeResponseExtractor<T> implements ResponseExtractor<T> {

    final Class<T> parameterClass;

    public StackExchangeResponseExtractor(Class parameterClass) {
        this.parameterClass = parameterClass;
    }

    @Override
    public T extractData(ClientHttpResponse response) throws IOException {
        String decompressedResponse = decompressGzip(response);

        return new ObjectMapper()
                .readValue(decompressedResponse, parameterClass);
    }

    private String decompressGzip(ClientHttpResponse response) throws IOException {
        InputStream responseBody = response.getBody();

        GZIPInputStream gzipInputStream = new GZIPInputStream(responseBody);
        BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInputStream, "UTF-8"));
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }

        reader.close();
        gzipInputStream.close();
        responseBody.close();

        return stringBuilder.toString();
    }
}
