package app.common.exchangeAPI;

import app.common.exchangeAPI.contract.GetSitesResponse;
import app.common.exchangeAPI.contract.Question;
import app.common.exchangeAPI.contract.SearchResponse;
import app.common.exchangeAPI.contract.Site;
import app.common.Settings;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Hashtable;
import java.util.Map;

@Component
public class StackExchangeAPIClient {

    final RestTemplate restTemplate;
    final String baseUrl;
    final Log logger;

    @Autowired
    public StackExchangeAPIClient(Settings settings) {

        logger = LogFactory.getLog(StackExchangeAPIClient.class);
        restTemplate = new RestTemplate();
        this.baseUrl = settings.getStackExchangeBaseURL();
    }

    public Site[] getSites() {

        String url = baseUrl + "sites?pagesize=100";

        StackExchangeResponseExtractor<GetSitesResponse> extractor = new StackExchangeResponseExtractor<>(GetSitesResponse.class);

        logger.debug("Executing a request " + url);
        return restTemplate.execute(url, HttpMethod.GET, null, extractor).items;
    }

    public Question[] search(String query, String site) {

        String url = baseUrl + "search?intitle={intitle}&site={site}";
        Map<String, String> uriVariables = getUriVariables(query, site);

        StackExchangeResponseExtractor<SearchResponse> extractor = new StackExchangeResponseExtractor(SearchResponse.class);

        logger.debug("Executing a request " + restTemplate.getUriTemplateHandler().expand(url, uriVariables));
        return restTemplate.execute(url, HttpMethod.GET, null, extractor, uriVariables).items;
    }

    private Map<String, String> getUriVariables(String query, String site) {

        Hashtable<String, String> result = new Hashtable<>();
        result.put("intitle", query);
        result.put("site", site);

        return result;
    }
}
