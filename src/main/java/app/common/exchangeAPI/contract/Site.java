package app.common.exchangeAPI.contract;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Site {

    public String name;
    public String api_site_parameter;

}
