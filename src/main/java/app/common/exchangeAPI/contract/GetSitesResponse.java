package app.common.exchangeAPI.contract;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSitesResponse {

    public Site[] items;

}
