package app.common.exchangeAPI.contract;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Question {

    public Owner owner;
    public boolean is_answered;
    public int creation_date;
    public String title;
    public String link;

}
